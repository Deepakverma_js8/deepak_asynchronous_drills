/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs = require('fs');

function createRandomJSON(){
    fs.mkdir('node',function(){  
        for(index=0; index<5; index++){
            // making empty file inside node directory
            fs.writeFile(`./node/myText${index}.JSON`,'',(err)=>{
                if(err) throw err;
                console.log("File has been saved!")
            })
            // deleting the above files 
            fs.unlink(`./node/myText${index}.JSON`,(err)=>{
                if(err) throw err;
                console.log('File has been Deleted!');
            });
            
        }
    })
}

module.exports=createRandomJSON ;
