const fs = require('fs')
function function1() {
    return new Promise((resolve, reject) => {
        fs.readFile('asynchronous-javascript/data/fs-callbacks/lipsum.txt', 'utf-8', (error, data) => {
            if (error) {
                reject(error)
            } else {
                resolve("Scuccessfully resolves function1")
            }
        })
    })
}

function function2() {
    return new Promise((resolve, reject) => {
        fs.readFile('asynchronous-javascript/data/fs-callbacks/lipsum.txt', 'utf-8', (error, data) => {
            if (error) {
                reject(error)
            }
            resolve(data)
        })
    })
    .then(data => {
        data = data.toUpperCase();
        return new Promise((resolve, reject) => {
            fs.writeFile('asynchronous-javascript/data/fs-callbacks/lipsumDataInUppercase.txt', data, (error) => {
                if (error) {
                    reject(error)
                }
                resolve('lipsumDataInUppercase.txt')
            })
        })
    })
    .then(data => {
        return new Promise((resolve, reject) => {
            fs.writeFile('asynchronous-javascript/data/fs-callbacks//filenames.txt', data, (error) => {
                if (error) {
                    reject(error)
                } else {
                    resolve("Successfully resolves function2")
                }
            })
        })
    }).catch(error => console.log(error))

}

function function3() {
    return new Promise((resolve, reject) => {
        fs.readFile('asynchronous-javascript/data/fs-callbacks/lipsumDataInUppercase.txt', 'utf-8', (error, data) => {
            if (error) {
                reject(error)
            } else {
                resolve(data)
            }
        })
    }).then(data => {
        data = data.toLowerCase();
        const result = data.match(/[^\.!\?]+[\.!\?]+/g)
        return new Promise((resolve, reject) => {
            fs.writeFile('asynchronous-javascript/data/fs-callbacks/splitSentences.txt', JSON.stringify(result, null, 4), (error) => {
                if (error) {
                    reject(error)
                } else {
                    resolve('splitSentences.txt')
                }
            })
        })
    })
    .then(data => {
        return new Promise((resolve, reject) => {
            fs.appendFile('asynchronous-javascript/data/fs-callbacks/filenames.txt', data, (error) => {
                if (error) {
                    reject(error)
                } else {
                    resolve("Successfully resolves function3")
                }
            })
        })
    })
    .catch((error) => console.log(error))
}

function function4() {
    return new Promise((resolve, reject) => {
        fs.readFile('asynchronous-javascript/data/fs-callbacks/splitSentences.txt', (error, data) => {
            if (error) {
                reject(error)
            } else {
                resolve(data)
            }
        })
    })
    .then(data => {
        return new Promise((resolve, reject) => {
            data = JSON.parse(data)
            fs.writeFile('asynchronous-javascript/data/fs-callbacks/sortedData.txt', JSON.stringify(data.sort(), null, 4), (error) => {
                if (error) {
                    reject(error)
                } else {
                    resolve("sortedData.txt")
                }
            })
        })
    })
    .then(data => {
        return new Promise((resolve, reject) => {
            fs.appendFile('asynchronous-javascript/data/fs-callbacks/filenames.txt', data, (error) => {
                if (error) {
                    reject(error)
                } else {
                    resolve("Successfully resolves function4")
                }
            })
        })
    })
    .catch(error => console.log(error))
}

function function5() {
    return new Promise((resolve, reject) => {
        fs.readFile('asynchronous-javascript/data/fs-callbacks/filenames.txt', (error, data) => {
            if (error) {
                reject(error)
            } else {
                resolve(data)
            }
        })
    })
    .then(data => {
        return new Promise((resolve, reject) => {
            const files = data.toString().split('.txt');
            for (let idx = 0; idx < files.length - 1; ++idx) {
                fs.unlink('asynchronous-javascript/data/fs-callbacks/' + files[idx] + '.txt', (error) => {
                    if (error) {
                        reject(error)
                    } else {
                        resolve("Successfully resolves the function5")
                    }
                })
            }
        })
    })
    .catch(error => console.log(error))
}

module.exports = { function1, function2 , function3 , function4 , function5 };
