/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID
    that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using 
    a callback function.
*/

const fs = require("fs")
const cardData = require('../data/trello-callbacks/cards.json');
function callback3(id) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (cardData.hasOwnProperty(id)) {
                resolve(cardData[id])
            } else {
                reject("Something wrong")
            }
        }, 2 * 1000)
    })
}

module.exports = callback3;
