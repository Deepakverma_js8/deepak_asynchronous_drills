/* 
	Problem 5: Write a function that will use the previously written functions to get the following information.
    You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const callback1 = require('./callback1')
const callback2 = require('./callback2')
const callback3 = require('./callback3')
const fs = require('fs')
const boardData = require('../data/trello-callbacks/boards.json')
function callback5(Thanos) {
        return new Promise((resolve, reject) => {
            const getThonasInfo = boardData.find(item => item.name === Thanos)
            if (getThonasInfo) {
                resolve(getThonasInfo['id'])
            } else {
                reject(error)
            }
        }).then(id => {

        return callback1(id)

    }).then(data => {

        return callback2(data.id)

    }).then(data => {

        const getInfo = data.filter(item => item.name === 'Mind' || item.name === 'Space')
        let promiseObj = []
        getInfo.forEach(item => {
            promiseObj.push(callback3(item.id))
        })

        return Promise.all(promiseObj)
    })

}

module.exports = callback5;

