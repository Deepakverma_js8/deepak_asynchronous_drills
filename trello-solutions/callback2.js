/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID
    that is passed to it from the given data in lists.json. Then pass control back to the code that called 
    it by using a callback function.
*/

const fs = require('fs')
const listData = require('../data/trello-callbacks/lists.json');
function callback2(id) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (listData.hasOwnProperty(id)) {
                resolve(listData[id])
            } else {
                reject("Something wrong")
            }
        }, 2 * 1000)
    })
}

module.exports = callback2;
